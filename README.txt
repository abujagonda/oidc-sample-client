OpenID Connect client tool

Copyright (c) Connect2id Ltd., 2012 - 2017


README

OpenID Connect client intended for testing, development and debugging of
relying parties, servers and applications.

This software is provided under the terms of the Apache 2.0 licence.


Questions or comments? Email support@connect2id.com


Acknowledgements

* Julian Krautwald, Vladislav Mladenov and Christian Mainka, security
  researchers at Horst Goertz Institute for IT-Security / Chair for Network and
  Data Security.

* Connect2id QA.


2017-01-05
