package com.nimbusds.openid.connect.client.vaadin;


import java.net.MalformedURLException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import javax.servlet.annotation.WebServlet;

import com.nimbusds.jose.Header;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.crypto.AESDecrypter;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jwt.*;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.assertions.jwt.JWTAssertionDetails;
import com.nimbusds.oauth2.sdk.assertions.jwt.JWTAssertionFactory;
import com.nimbusds.oauth2.sdk.auth.*;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.jose.SecretKeyDerivation;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import com.nimbusds.oauth2.sdk.util.URLUtils;
import com.nimbusds.openid.connect.sdk.*;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.validators.IDTokenValidator;
import com.thetransactioncompany.json.pretty.PrettyJson;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * OpenID authentication response callback (redirect_uri target).
 */
public class Callback extends UI {
	
	
	@WebServlet(urlPatterns = "/cb", name = "CallbackServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = Callback.class, productionMode = false)
	public static class CallbackServlet extends HTTPRequestParametersInterceptorServlet {
	}
	
	
	/**
	 * The logger.
	 */
	private static Logger log = LogManager.getLogger("CALLBACK");
	
	
	/**
	 * Wraps a string at 80 characters.
	 *
	 * @param s The string to wrap. Must not be {@code null}.
	 *
	 * @return The wrapped string with line breaks at 80 characters.
	 */
	private static String wrapLongString(final String s) {
		
		return WordUtils.wrap(s, 80, "\n", true);
	}
	
	
	/**
	 * Creates an error message panel.
	 *
	 * @param errorMessage The error message. Must not be {@code null}.
	 *
	 * @return The panel.
	 */
	private static Panel createErrorMessagePanel(final String errorMessage) {
		
		Panel panel = new Panel("Error");
		
		// Always log the error message
		log.info("Error: {}", errorMessage);
		
		VerticalLayout content = new VerticalLayout();
		content.setMargin(true);
		content.setSpacing(true);
		content.addComponent(new Label(errorMessage));
		panel.setContent(content);
		return panel;
	}
	
	
	/**
	 * Creates a layout to display an OAuth 2.0 error object.
	 *
	 * @param errorObject The error object. Must not be {@code null}.
	 *
	 * @return The layout.
	 */
	private static Layout createErrorObjectLayout(final ErrorObject errorObject) {
		
		GridLayout grid = new GridLayout(2, 3);
		grid.setMargin(true);
		grid.setSpacing(true);
		
		grid.addComponent(new Label("Error code"), 0, 0);
		grid.addComponent(new Label(errorObject.getCode()), 1, 0);
		
		if (errorObject.getDescription() != null) {
			grid.addComponent(new Label("Error description"), 0, 1);
			grid.addComponent(new Label(errorObject.getDescription()), 1, 1);
		}
		
		if (errorObject.getURI() != null) {
			grid.addComponent(new Label("Error URI"), 0, 2);
			grid.addComponent(new Label(errorObject.getURI() + ""), 1, 2);
		}
		
		return grid;
	}
	
	
	/**
	 * Creates a panel to display an OpenID authentication error response.
	 *
	 * @param errorResponse The OpenID authentication error response. Must
	 *                      not be {@code null}.
	 *
	 * @return The panel.
	 */
	private static Panel createAuthzErrorPanel(final AuthenticationErrorResponse errorResponse) {
		
		Panel panel = new Panel("OpenID authentication response");
		
		ErrorObject errorObject = errorResponse.getErrorObject();
		
		if (errorObject != null) {
			panel.setContent(createErrorObjectLayout(errorObject));
		}
		
		return panel;
	}
	
	
	/**
	 * Creates a panel to display the result of a successful OpenID
	 * authentication response.
	 *
	 * @param successResponse The OpenID authentication success response.
	 *
	 * @return The panel.
	 */
	private static Panel createAuthzSuccessPanel(final AuthenticationSuccessResponse successResponse) {
		
		Panel panel = new Panel("OpenID authentication response");
		
		GridLayout grid = new GridLayout(2, 1);
		grid.setMargin(true);
		grid.setSpacing(true);
		
		int row = 0;
		
		if (successResponse.getAuthorizationCode() != null) {
			grid.addComponent(new Label("Authorization code"), 0, row);
			Label codeLabel = new Label(successResponse.getAuthorizationCode().getValue());
			codeLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(codeLabel, 1, row);
		}
		
		if (successResponse.getIDToken() != null) {
			grid.insertRow(++row);
			grid.addComponent(new Label("ID token"), 0, row);
			Label idTokenLabel = new Label(wrapLongString(successResponse.getIDToken().getParsedString()));
			idTokenLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(idTokenLabel, 1, row);
		}
		
		if (successResponse.getAccessToken() != null) {
			grid.insertRow(++row);
			grid.addComponent(new Label("Access token"), 0, row);
			Label tokenLabel = new Label(wrapLongString(successResponse.getAccessToken().getValue()));
			tokenLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(tokenLabel, 1, row);
		}
		
		if (successResponse.getState() != null) {
			grid.insertRow(++row);
			grid.addComponent(new Label("State"), 0, row);
			Label stateLabel = new Label(successResponse.getState().getValue());
			stateLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(stateLabel, 1, row);
		}
		
		panel.setContent(grid);
		
		return panel;
	}
	
	
	/**
	 * Creates a panel to display an ID token with its header, claims set
	 * and signature.
	 *
	 * @param idToken   The ID token. Must be JWS or plain. Not
	 *                  {@code null}.
	 * @param jweHeader The JWE header is additional encryption has been
	 *                  applied.
	 *
	 *
	 * @return The panel.
	 */
	private static Panel createIDTokenPanel(final JWT idToken, final JWEHeader jweHeader) {
		
		Panel panel = new Panel("ID token");
		
		GridLayout grid = new GridLayout(2, jweHeader != null ? 4 : 3);
		grid.setMargin(true);
		grid.setSpacing(true);
		
		PrettyJson jsonFormatter = new PrettyJson(PrettyJson.Style.COMPACT);
		
		String formattedJWEHeader = null;
		String formattedJWSHeader = null;
		String formattedClaims = null;
		String formattedSignature = null;
		
		try {
			formattedJWEHeader = jweHeader != null ? jsonFormatter.parseAndFormat(jweHeader.toJSONObject().toJSONString()) : null;
			formattedJWSHeader = jsonFormatter.parseAndFormat(idToken.getHeader().toJSONObject().toJSONString());
			formattedClaims = jsonFormatter.parseAndFormat(idToken.getJWTClaimsSet().toJSONObject().toJSONString());
			formattedSignature = wrapLongString(((SignedJWT) idToken).getSignature().toString());
			
		} catch (Exception e) {
			// unlikely
			throw new RuntimeException("Failed to format ID token: " + e.getMessage());
		}
		
		int row = 0;
		
		if (formattedJWEHeader != null) {
			grid.addComponent(new Label("JWE Header"), 0, row);
			Label jweHeaderLabel = new Label(formattedJWEHeader);
			jweHeaderLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(jweHeaderLabel, 1, row);
			row++;
		}
		
		grid.addComponent(new Label("JWS Header"), 0, row);
		Label jwsHeaderLabel = new Label(formattedJWSHeader);
		jwsHeaderLabel.setContentMode(ContentMode.PREFORMATTED);
		grid.addComponent(jwsHeaderLabel, 1, row);
		row++;
		
		grid.addComponent(new Label("Claims"), 0, row);
		Label claimsLabel = new Label(formattedClaims);
		claimsLabel.setContentMode(ContentMode.PREFORMATTED);
		grid.addComponent(claimsLabel, 1, row);
		row++;
		
		grid.addComponent(new Label("Signature"), 0, row);
		Label signatureLabel = new Label(formattedSignature);
		signatureLabel.setContentMode(ContentMode.PREFORMATTED);
		grid.addComponent(signatureLabel, 1, row);
		
		panel.setContent(grid);
		
		return panel;
	}
	
	
	/**
	 * Creates a panel to display the OpenID provider's public signing
	 * key in JSON Web Key (JWK) format.
	 *
	 * @param jwk The public JWK. Must not be {@code null}.
	 *
	 * @return The panel.
	 */
	private static Panel createSigningJWKPanel(final JWK jwk) {
		
		Panel panel = new Panel("Provider public " + jwk.getKeyType() + " JSON Web Key (JWK)");
		
		GridLayout grid = new GridLayout(2, 1);
		grid.setMargin(true);
		grid.setSpacing(true);
		
		int row = 0;
		
		grid.addComponent(new Label("Key type"), 0, row);
		Label typeLabel = new Label(jwk.getKeyType().getValue());
		typeLabel.setContentMode(ContentMode.PREFORMATTED);
		grid.addComponent(typeLabel, 1, row);
		
		if (jwk instanceof RSAKey) {
			
			grid.insertRow(++row);
			grid.addComponent(new Label("Modulus (n)"), 0, row);
			Label nLabel = new Label(wrapLongString(((RSAKey) jwk).getModulus().toString()));
			nLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(nLabel, 1, row);
			
			grid.insertRow(++row);
			grid.addComponent(new Label("Public exponent (e)"), 0, row);
			Label eLabel = new Label(wrapLongString(((RSAKey) jwk).getPublicExponent().toString()));
			eLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(eLabel, 1, row);
			
		} else if (jwk instanceof ECKey) {
			
			grid.insertRow(++row);
			grid.addComponent(new Label("Curve"), 0, row);
			Label curveLabel = new Label(wrapLongString(((ECKey) jwk).getCurve().toString()));
			curveLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(curveLabel, 1, row);
			
			grid.insertRow(++row);
			grid.addComponent(new Label("X coordinate"), 0, row);
			Label xLabel = new Label(wrapLongString(((ECKey) jwk).getX().toString()));
			xLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(xLabel, 1, row);
			
			grid.insertRow(++row);
			grid.addComponent(new Label("Y coordinate"), 0, row);
			Label yLabel = new Label(wrapLongString(((ECKey) jwk).getY().toString()));
			yLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(yLabel, 1, row);
		}
		
		if (jwk.getKeyID() != null) {
			grid.insertRow(++row);
			grid.addComponent(new Label("Key ID"), 0, row);
			Label idLabel = new Label(jwk.getKeyID());
			idLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(idLabel, 1, row);
		}
		
		if (jwk.getKeyUse() != null) {
			grid.insertRow(++row);
			grid.addComponent(new Label("Key use"), 0, row);
			Label useLabel = new Label(jwk.getKeyUse().name());
			useLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(useLabel, 1, row);
		}
		
		panel.setContent(grid);
	
		return panel;
	}
	
	
	/**
	 * Creates a panel to display a successful token response.
	 *
	 * @param tokenResponse The token response. Must not be {@code null}.
	 *
	 * @return The panel.
	 */
	private static Panel createTokenResponsePanel(final OIDCTokenResponse tokenResponse) {
		
		Panel panel = new Panel("Token response");
		
		GridLayout grid = new GridLayout(2, 1);
		grid.setMargin(true);
		grid.setSpacing(true);
		
		int row = 0;
		
		JWT idToken = tokenResponse.getOIDCTokens().getIDToken();
		if (idToken != null && idToken.getParsedString() != null) {
			grid.addComponent(new Label("ID token"), 0, row);
			Label idTokenValue = new Label(wrapLongString(idToken.getParsedString()));
			idTokenValue.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(idTokenValue, 1, row);
		}
		
		AccessToken accessToken = tokenResponse.getOIDCTokens().getAccessToken();
		if (accessToken != null) {
			grid.insertRow(++row);
			grid.addComponent(new Label("Access token type"), 0, row);
			Label typeLabel = new Label(accessToken.getType().getValue());
			typeLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(typeLabel, 1, row);
			
			BearerAccessToken bat = (BearerAccessToken)accessToken;
			
			grid.insertRow(++row);
			grid.addComponent(new Label("Access token value"), 0, row);
			Label valueLabel = new Label(wrapLongString(bat.getValue()));
			valueLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(valueLabel, 1, row);
			
			grid.insertRow(++row);
			grid.addComponent(new Label("Access token lifetime"), 0, row);
			grid.addComponent(new Label(bat.getLifetime() + " seconds"), 1, row);
			
			if (bat.getScope() != null) {
				grid.insertRow(++row);
				grid.addComponent(new Label("Access token scope"), 0, row);
				Label scopeLabel = new Label(wrapLongString(bat.getScope().toString()));
				scopeLabel.setContentMode(ContentMode.PREFORMATTED);
				grid.addComponent(scopeLabel, 1, row);
			}
		}
		
		RefreshToken refreshToken = tokenResponse.getOIDCTokens().getRefreshToken();
		
		if (refreshToken != null) {
			grid.insertRow(++row);
			grid.addComponent(new Label("Refresh token"), 0, row);
			Label valueLabel = new Label(refreshToken.getValue());
			valueLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(valueLabel, 1, row);
		}
		
		panel.setContent(grid);
		
		return panel;
	}
	
	
	/**
	 * Creates a panel to display a token error response.
	 *
	 * @param httpStatusCode The HTTP status code.
	 * @param errorResponse  The token error response. Must not be
	 *                       {@code null}.
	 *
	 * @return The panel.
	 */
	private static Panel createTokenErrorPanel(final int httpStatusCode, final TokenErrorResponse errorResponse) {
		
		Panel panel = new Panel("Token error response (HTTP " + httpStatusCode + ")");
		
		log.info("Token error: HTTP " + httpStatusCode);
		
		ErrorObject errorObject = errorResponse.getErrorObject();
		
		if (errorObject != null) {
			panel.setContent(createErrorObjectLayout(errorObject));
		}
		
		return panel;
	}
	
	
	/**
	 * Creates a panel to display the obtained UserInfo.
	 *
	 * @param jweHeader Optional JWE header for a UserInfo JWT,
	 *                  {@code null} it not applicable.
	 * @param jwsHeader Optional JWS / plain header for a UserInfo JWT,
	 *                  {@code null} it not applicable.
	 * @param userInfo  The user information as a JSON object. Must not be
	 *                  {@code null}.
	 *
	 * @return The panel.
	 */
	private static Panel createUserInfoPanel(final JWEHeader jweHeader,
						 final Header jwsHeader,
						 final JSONObject userInfo)
		throws java.text.ParseException {
		
		
		Panel panel = new Panel("UserInfo");
		
		int numRows = 1;
		if (jweHeader != null) numRows++;
		if (jwsHeader != null) numRows++;
		
		GridLayout grid = new GridLayout(2, numRows);
		grid.setMargin(true);
		grid.setSpacing(true);
		
		PrettyJson jsonFormatter = new PrettyJson(PrettyJson.Style.COMPACT);
		
		int row = 0;
		
		if (jweHeader != null) {
			grid.addComponent(new Label("JWE Header"), 0, row);
			Label jweHeaderLabel = new Label(jsonFormatter.parseAndFormat(jweHeader.toJSONObject().toJSONString()));
			jweHeaderLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(jweHeaderLabel, 1, row);
			row++;
		}
		
		if (jwsHeader != null) {
			grid.addComponent(new Label("JWS Header"), 0, row);
			Label jwsHeaderLabel = new Label(jsonFormatter.parseAndFormat(jwsHeader.toJSONObject().toJSONString()));
			jwsHeaderLabel.setContentMode(ContentMode.PREFORMATTED);
			grid.addComponent(jwsHeaderLabel, 1, row);
			row++;
		}
		
		grid.addComponent(new Label("Claims"), 0, row);
		Label claimsLabel = new Label(jsonFormatter.parseAndFormat(userInfo.toJSONString()));
		claimsLabel.setContentMode(ContentMode.PREFORMATTED);
		grid.addComponent(claimsLabel, 1, row);
		
		panel.setContent(grid);
		
		return panel;
	}
	
	
	/**
	 * Decrypts the specified JWT with a shared key derived from a client
	 * secret.
	 *
	 * @param encryptedJWT The JWT to decrypt. Must not be {@code null}.
	 * @param clientSecret The client secret.
	 *
	 * @throws JOSEException If decryption failed.
	 */
	private static void decrypt(final EncryptedJWT encryptedJWT, final Secret clientSecret)
		throws JOSEException {
		
		if (clientSecret == null) {
			throw new JOSEException("Missing client_secret");
		}
		
		if (AESDecrypter.SUPPORTED_ALGORITHMS.contains(encryptedJWT.getHeader().getAlgorithm())) {
			
			encryptedJWT.decrypt(new AESDecrypter(SecretKeyDerivation.deriveSecretKey(
				clientSecret,
				encryptedJWT.getHeader().getAlgorithm(),
				encryptedJWT.getHeader().getEncryptionMethod())));
			
		} else if (DirectDecrypter.SUPPORTED_ALGORITHMS.contains(encryptedJWT.getHeader().getAlgorithm())) {
			
			encryptedJWT.decrypt(new DirectDecrypter(SecretKeyDerivation.deriveSecretKey(
				clientSecret,
				encryptedJWT.getHeader().getAlgorithm(),
				encryptedJWT.getHeader().getEncryptionMethod())));
		} else {
			throw new JOSEException("Unsupported JWE encryption algorithm: " + encryptedJWT.getHeader().getAlgorithm());
		}
	}
	
	
	@Override
	protected void init(final VaadinRequest vaadinRequest) {
		
		VerticalLayout content = new VerticalLayout();
		content.setMargin(true);
		content.setSpacing(true);
		setContent(content);
		
		// Vaadin sanity check
		if (VaadinService.getCurrentRequest().getWrappedSession(false) == null) {
			content.addComponent(createErrorMessagePanel("No active browser session"));
			return;
		}
		
		final Map<String,String> params;
		
		String fragment = getPage().getUriFragment();
		
		if (StringUtils.isNotBlank(fragment)) {
			// The response is encoded in the URI fragment
			params = URLUtils.parseParameters(fragment);
			
		} else {
			// The response is encoded in the URI query string, or POST form
			params = (Map<String,String>)vaadinRequest.getWrappedSession().getAttribute("http-request-parameters");
		}
		
		AuthenticationResponse oidcResponse;
		try {
			// Parse the OIDC response from collected parameters
			oidcResponse = AuthenticationResponseParser.parse(new URI("https:///"), params);
		} catch (Exception e) {
			content.addComponent(createErrorMessagePanel("Invalid OpenID authentication response: " + e.getMessage()));
			return;
		}
		
		if (oidcResponse.indicatesSuccess()) {
			// Display success parameters
			content.addComponent(createAuthzSuccessPanel((AuthenticationSuccessResponse)oidcResponse));
		} else {
			// Display error parameters
			content.addComponent(createAuthzErrorPanel((AuthenticationErrorResponse) oidcResponse));
		}
		
		
		// Get the client map of pending OpenID requests
		Map<State,PendingAuthenticationRequest> pendingRequests = (Map<State,PendingAuthenticationRequest>)VaadinService.getCurrentRequest().getWrappedSession().getAttribute("pending-requests");
		
		if (pendingRequests == null) {
			// Hostname of callback doesn't match the URL from which the client was launched
			content.addComponent(createErrorMessagePanel("Unexpected callback: " +
				"The OpenID client was started from a hostname / IP address that differs from the hostname / IP address of the redirect_uri"));
			return;
		}
		
		if (oidcResponse.getState() == null) {
			// State is recommended in OAuth 2.0, but mandatory in OpenID Connect
			content.addComponent(createErrorMessagePanel("Invalid OpenID authentication response: Missing state parameter"));
			return;
		}
		
		PendingAuthenticationRequest pendingRequest = pendingRequests.get(oidcResponse.getState());
		if (pendingRequest == null) {
			content.addComponent(createErrorMessagePanel("Unexpected OpenID authentication response: " +
				"No pending OpenID authentication request with state " + oidcResponse.getState()));
			return;
		}
		
		// If error stop here
		if (! oidcResponse.indicatesSuccess()) {
			return;
		}
		
		AuthenticationSuccessResponse successResponse = (AuthenticationSuccessResponse)oidcResponse;
		
		try {
			log.info("Received OpenID authentication success response: " + successResponse.toParameters());
		} catch (Exception e) {
			// Just in case toParameters fails
			content.addComponent(createErrorMessagePanel(e.getMessage()));
			return;
		}
		
		// Process authorization code if expected
		if (pendingRequest.getAuthenticationRequest().getResponseType().contains("code") && successResponse.getAuthorizationCode() == null) {
			content.addComponent(createErrorMessagePanel("Bad OpenID authentication response: Missing expected authorization code, aborting further processing"));
			return;
		}
		
		AuthorizationCode authzCode = successResponse.getAuthorizationCode();
		AccessToken accessToken = successResponse.getAccessToken();
		JWT idToken = successResponse.getIDToken();
		
		if (authzCode != null) {
			
			// Make token endpoint request
			URI tokenEndpointURI = pendingRequest.getProviderMetadata().getTokenEndpointURI();
			OIDCClientInformation clientInfo = pendingRequest.getClientInfo();
			
			AuthorizationGrant authzGrant = new AuthorizationCodeGrant(
				authzCode,
				pendingRequest.getAuthenticationRequest().getRedirectionURI(),
				pendingRequest.getCodeVerifier());
			
			TokenRequest tokenRequest;
			
			if (clientInfo.getOIDCMetadata().getTokenEndpointAuthMethod().equals(ClientAuthenticationMethod.NONE)) {
				// Public client
				tokenRequest = new TokenRequest(tokenEndpointURI, clientInfo.getID(), authzGrant);
			} else {
				// Confidential client
				ClientAuthenticationMethod expectedAuthMethod = clientInfo.getOIDCMetadata().getTokenEndpointAuthMethod();
				
				final ClientAuthentication clientAuth;
				
				if (ClientAuthenticationMethod.CLIENT_SECRET_BASIC.equals(expectedAuthMethod)) {
					clientAuth = new ClientSecretBasic(clientInfo.getID(), clientInfo.getSecret());
				} else if (ClientAuthenticationMethod.CLIENT_SECRET_JWT.equals(expectedAuthMethod)) {
					try {
						clientAuth = new ClientSecretJWT(JWTAssertionFactory.create(
							new JWTAssertionDetails(
								new Issuer(clientInfo.getID().getValue()),
								new Subject(clientInfo.getID().getValue()),
								new Audience(tokenEndpointURI.toString())),
							clientInfo.getOIDCMetadata().getTokenEndpointAuthJWSAlg(),
							clientInfo.getSecret()));
					} catch (JOSEException e) {
						content.addComponent(createErrorMessagePanel(e.getMessage()));
						return;
					}
				} else if (ClientAuthenticationMethod.CLIENT_SECRET_POST.equals(expectedAuthMethod)) {
					clientAuth = new ClientSecretPost(clientInfo.getID(), clientInfo.getSecret());
				} else {
					content.addComponent(createErrorMessagePanel("Unsupported client authentication method: " + expectedAuthMethod));
					return;
				}
				tokenRequest = new TokenRequest(tokenEndpointURI, clientAuth, authzGrant);
			}
			
			try {
				HTTPRequest httpRequest = tokenRequest.toHTTPRequest();
				
				HTTPResponse httpResponse = httpRequest.send();
				
				TokenResponse tokenResponse = OIDCTokenResponseParser.parse(httpResponse);
				
				if (tokenResponse instanceof TokenErrorResponse) {
					
					int httpStatusCode = httpResponse.getStatusCode();
					content.addComponent(createTokenErrorPanel(httpStatusCode, (TokenErrorResponse)tokenResponse));
					return;
				}
				
				OIDCTokenResponse oidcTokenResponse = (OIDCTokenResponse)tokenResponse;
				
				accessToken = oidcTokenResponse.getOIDCTokens().getAccessToken();
				log.info("Access token: " + accessToken.getValue());
				
				if (oidcTokenResponse.getOIDCTokens().getIDToken() != null) {
					idToken = oidcTokenResponse.getOIDCTokens().getIDToken();
				}
				
				content.addComponent(createTokenResponsePanel(oidcTokenResponse));
				
			} catch (Exception e) {
				content.addComponent(createErrorMessagePanel("Couldn't make token request: " + e.getMessage()));
				return;
			}
		}
		
		
		// Validate ID token
		if (idToken == null) {
			content.addComponent(createErrorMessagePanel("ID token missing from response, aborting further processing"));
			return;
		}
		
		JWEHeader jweHeader = null;
		
		if (idToken instanceof EncryptedJWT) {
			
			EncryptedJWT encryptedIDToken = (EncryptedJWT)idToken;
			jweHeader = encryptedIDToken.getHeader();
			try {
				decrypt(encryptedIDToken, pendingRequest.getClientInfo().getSecret());
				idToken = JWTParser.parse(encryptedIDToken.getPayload().toString());
			} catch (JOSEException | java.text.ParseException e) {
				content.addComponent(createErrorMessagePanel("Couldn't decrypt ID token: " + e.getMessage()));
				return;
			}
		}
		
		IDTokenValidator validator;
		
		if (idToken instanceof SignedJWT) {
			
			if (JWSAlgorithm.Family.SIGNATURE.contains(idToken.getHeader().getAlgorithm())) {
				
				try {
					validator = new IDTokenValidator(
						pendingRequest.getProviderMetadata().getIssuer(),
						pendingRequest.getClientInfo().getID(),
						JWSAlgorithm.parse(idToken.getHeader().getAlgorithm().getName()),
						pendingRequest.getProviderMetadata().getJWKSetURI().toURL());
				} catch (MalformedURLException e) {
					content.addComponent(createErrorMessagePanel("Invalid JWK set URL: " + e.getMessage()));
					return;
				}
				
				// Display the remote signing JWK for the ID token
				try {
					List<JWK> candidates = new RemoteJWKSet(pendingRequest.getProviderMetadata().getJWKSetURI().toURL())
						.get(new JWKSelector(new JWKMatcher.Builder()
							.keyType(KeyType.forAlgorithm(idToken.getHeader().getAlgorithm()))
							.keyID(((SignedJWT)idToken).getHeader().getKeyID())
							.build()),
							null);
					
					if (candidates.isEmpty()) {
						content.addComponent(createErrorMessagePanel("No matching signing JWK found"));
						return;
					}
					
					content.addComponent(createSigningJWKPanel(candidates.get(0)));
					
				} catch (Exception e) {
					content.addComponent(createErrorMessagePanel("Couldn't retrieve OpenID provider JWK set: " + e.getMessage()));
					return;
				}
				
			} else if (JWSAlgorithm.Family.HMAC_SHA.contains(idToken.getHeader().getAlgorithm())) {
				
				if (pendingRequest.getClientInfo().getSecret() == null) {
					content.addComponent(createErrorMessagePanel("Cannot validate HMAC ID token: Missing client secret"));
					return;
				}
				
				validator = new IDTokenValidator(
					pendingRequest.getProviderMetadata().getIssuer(),
					pendingRequest.getClientInfo().getID(),
					JWSAlgorithm.parse(idToken.getHeader().getAlgorithm().getName()),
					pendingRequest.getClientInfo().getSecret());
				
			} else {
				content.addComponent(createErrorMessagePanel("Unsupported JWS algorithm: " + idToken.getHeader().getAlgorithm()));
				return;
			}
			
		} else if (idToken instanceof PlainJWT) {
			
			validator = new IDTokenValidator(pendingRequest.getProviderMetadata().getIssuer(), pendingRequest.getClientInfo().getID());
			
		} else {
			content.addComponent(createErrorMessagePanel("Unsupported nested JWT: " + idToken.getParsedString()));
			return;
		}
		
		
		try {
			validator.validate(idToken, pendingRequest.getAuthenticationRequest().getNonce());
			
		} catch (Exception e) {
			content.addComponent(createErrorMessagePanel("Invalid ID token: " + e.getMessage()));
		}
		
		content.addComponent(createIDTokenPanel(idToken, jweHeader));
		
		if (accessToken != null) {
			
			// Make UserInfo request
			URI userInfoEndpoint = pendingRequest.getProviderMetadata().getUserInfoEndpointURI();
			
			log.info("Making UserInfo request to " + userInfoEndpoint);
			
			UserInfoRequest userInfoRequest = new UserInfoRequest(userInfoEndpoint, (BearerAccessToken)accessToken);
			
			try {
				HTTPRequest httpRequest = userInfoRequest.toHTTPRequest();
				
				HTTPResponse httpResponse = httpRequest.send();
				
				UserInfoResponse userInfoResponse = UserInfoResponse.parse(httpResponse);
				
				if (userInfoResponse instanceof UserInfoErrorResponse) {
					
					UserInfoErrorResponse userInfoErrorResponse = (UserInfoErrorResponse)userInfoResponse;
					
					String msg = "[ " + userInfoErrorResponse.getErrorObject().getCode() + " ] ";
					msg += userInfoErrorResponse.getErrorObject().getDescription();
					
					content.addComponent(createErrorMessagePanel("UserInfo error response: " + msg));
				}
				
				UserInfoSuccessResponse userInfoSuccessResponse = (UserInfoSuccessResponse)userInfoResponse;
				
				JWEHeader userInfoJWEHeader = null;
				Header userInfoJWSHeader = null;
				JSONObject jsonObject;
				
				if (userInfoSuccessResponse.getUserInfoJWT() != null) {
					
					JWT jwt = userInfoSuccessResponse.getUserInfoJWT();
					
					if (jwt instanceof EncryptedJWT) {
						EncryptedJWT encryptedJWT = (EncryptedJWT)jwt;
						userInfoJWEHeader = encryptedJWT.getHeader();
						decrypt(encryptedJWT, pendingRequest.getClientInfo().getSecret());
						jwt = JWTParser.parse(encryptedJWT.getPayload().toString());
					}
					
					userInfoJWSHeader = jwt.getHeader();
					jsonObject = jwt.getJWTClaimsSet().toJSONObject();
					
				} else {
					
					jsonObject = userInfoSuccessResponse.getUserInfo().toJSONObject();
				}
				
				content.addComponent(createUserInfoPanel(userInfoJWEHeader, userInfoJWSHeader, jsonObject));
				
			} catch (Exception e) {
				content.addComponent(createErrorMessagePanel("Couldn't make UserInfo request: " + e.getMessage()));
			}
		}
	}
}
