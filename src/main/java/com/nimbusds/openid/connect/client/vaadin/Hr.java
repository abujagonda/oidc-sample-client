package com.nimbusds.openid.connect.client.vaadin;


import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;


/**
 * Horizontal ruler.
 */
class Hr extends Label {
	
	Hr() {
		super("<hr style=\"border-top: 0px\"/>", ContentMode.HTML);
	}
}