package com.nimbusds.openid.connect.client.vaadin;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.servlet.annotation.WebServlet;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.ResponseMode;
import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.pkce.CodeChallenge;
import com.nimbusds.oauth2.sdk.pkce.CodeChallengeMethod;
import com.nimbusds.oauth2.sdk.pkce.CodeVerifier;
import com.nimbusds.openid.connect.sdk.*;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property;
import com.vaadin.server.*;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.apache.commons.lang3.StringUtils;


/**
 * OpenID Connect client for testing and development purposes.
 */
@Theme("mytheme")
@SuppressWarnings("serial")
public class OIDCClient extends UI {
	
	
	@WebServlet(urlPatterns = "/*", name = "OIDCClientServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = OIDCClient.class, productionMode = false)
	public static class OIDCClientServlet extends VaadinServlet {
	}
	
	
	/**
	 * The OpenID provider, relying party and request parameters.
	 */
	private final OIDCParameters oidcParameters;
	
	
	/**
	 * Pop-up opener for redirecting to the authorisation endpoint of the
	 * OP with an authentication request. The URL is set each time the form
	 * is updated.
	 */
	private final BrowserWindowOpener popupOpener = new BrowserWindowOpener("");
	
	
	/**
	 * Listener for updating the composed OpenID authentication request.
	 */
	private final Property.ValueChangeListener valueChangeListener;
	
	
	/**
	 * Stores pending (in progress) OpenID authentication requests keyed by
	 * their state parameter.
	 */
	private final Map<State,PendingAuthenticationRequest> pendingRequests = new Hashtable<>();
	
	
	/**
	 * Creates a new main window for the OpenID Connect client.
	 */
	public OIDCClient() {
		
		// Export to session
		VaadinService.getCurrentRequest().getWrappedSession().setAttribute("pending-requests", pendingRequests);
		
		oidcParameters = new OIDCParameters();
		
		valueChangeListener = (Property.ValueChangeListener) valueChangeEvent -> {
			AuthenticationRequest authRequest = buildAuthenticationRequest();
			if (authRequest == null) {
				return;
			}
			String uriString = authRequest.toURI().toString();
			Loggers.MAIN.debug("Updating OpenID authentication request: " + uriString);
			popupOpener.setUrl(uriString);
		};
	}
	
	
	/**
	 * Creates the form to enter the OpenID provider issuer and protocol
	 * endpoints.
	 *
	 * @return The form layout.
	 */
	@SuppressWarnings("unchecked")
	private FormLayout createProviderDetailsForm() {
		
		FormLayout form = new FormLayout();
		form.setMargin(true);
		form.setSpacing(true);
		
		final int URI_FIELD_COLS = 25;
		final URIValidator urlValidator = new URIValidator();
		
		TextField opIssuerField = new TextField("Issuer");
		opIssuerField.setRequired(true);
		opIssuerField.setImmediate(true);
		opIssuerField.setColumns(URI_FIELD_COLS);
		opIssuerField.addValidator(urlValidator);
		opIssuerField.setPropertyDataSource(oidcParameters.getProviderProperties().getItemProperty("iss"));
		form.addComponent(opIssuerField);
		
		HorizontalLayout queryEndpointsLayout = new HorizontalLayout();
		queryEndpointsLayout.setCaption("Endpoints");
		Button queryEndpointsButton = new Button("Query");
		queryEndpointsLayout.addComponent(queryEndpointsButton);
		queryEndpointsLayout.addComponent(new Label("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", ContentMode.HTML)); // need extra spacing here
		CheckBox endpointsEditManuallyCheckbox = new CheckBox("Edit manually");
		queryEndpointsLayout.addComponent(endpointsEditManuallyCheckbox);
		queryEndpointsLayout.setComponentAlignment(endpointsEditManuallyCheckbox, Alignment.MIDDLE_RIGHT);
		form.addComponent(queryEndpointsLayout);
		
		// OP endpoints
		List<TextField> opEndpoints = new LinkedList<>();
		
		TextField jwksEndpointField = new TextField("JSON Web Key set");
		jwksEndpointField.setReadOnly(true);
		jwksEndpointField.setRequired(true);
		jwksEndpointField.setImmediate(true);
		jwksEndpointField.setColumns(URI_FIELD_COLS);
		jwksEndpointField.addValidator(urlValidator);
		jwksEndpointField.setPropertyDataSource(oidcParameters.getProviderProperties().getItemProperty("jwks_uri"));
		jwksEndpointField.addValueChangeListener(valueChangeListener);
		opEndpoints.add(jwksEndpointField);
		form.addComponent(jwksEndpointField);
		
		TextField authzEndpointField = new TextField("Authorization endpoint");
		authzEndpointField.setReadOnly(true);
		authzEndpointField.setRequired(true);
		authzEndpointField.setImmediate(true);
		authzEndpointField.setColumns(URI_FIELD_COLS);
		authzEndpointField.addValidator(urlValidator);
		authzEndpointField.setPropertyDataSource(oidcParameters.getProviderProperties().getItemProperty("authz_uri"));
		authzEndpointField.addValueChangeListener(valueChangeListener);
		opEndpoints.add(authzEndpointField);
		form.addComponent(authzEndpointField);
		
		TextField tokenEndpointField = new TextField("Token endpoint");
		tokenEndpointField.setReadOnly(true);
		tokenEndpointField.setRequired(true);
		tokenEndpointField.setImmediate(true);
		tokenEndpointField.setColumns(URI_FIELD_COLS);
		tokenEndpointField.addValidator(urlValidator);
		tokenEndpointField.setPropertyDataSource(oidcParameters.getProviderProperties().getItemProperty("token_uri"));
		tokenEndpointField.addValueChangeListener(valueChangeListener);
		opEndpoints.add(tokenEndpointField);
		form.addComponent(tokenEndpointField);
		
		TextField userInfoEndpointField = new TextField("UserInfo endpoint");
		userInfoEndpointField.setReadOnly(true);
		userInfoEndpointField.setRequired(true);
		userInfoEndpointField.setImmediate(true);
		userInfoEndpointField.setColumns(URI_FIELD_COLS);
		userInfoEndpointField.addValidator(urlValidator);
		userInfoEndpointField.setPropertyDataSource(oidcParameters.getProviderProperties().getItemProperty("userinfo_uri"));
		userInfoEndpointField.addValueChangeListener(valueChangeListener);
		opEndpoints.add(userInfoEndpointField);
		form.addComponent(userInfoEndpointField);
		
		// Actions
		Page currentPage = this.getPage();
		
		// Query OP metadata
		queryEndpointsButton.addClickListener((Button.ClickListener) clickEvent -> {
			
			String issValue = (String)oidcParameters.getProviderProperties().getItemProperty("iss").getValue();
			
			Loggers.MAIN.debug("Input OP issuer: {}", issValue);
			
			oidcParameters.clearProviderEndpoints();
			
			URL issURL;
			try {
				issURL = new URL(issValue);
			} catch (MalformedURLException e) {
				new Notification(
					"Invalid OpenID Provider Issuer",
					"<p>Must be a valid HTTP(s) URL, e.g. https://demo.c2id.com/c2id</p>",
					Notification.Type.WARNING_MESSAGE,
					true)
					.show(currentPage);
				return;
			}
			
			URL opMetadataURL = OIDCProviderMetadataRetriever.composeMetadataURL(issURL);
			
			OIDCProviderMetadata opMetadata;
			try {
				opMetadata = OIDCProviderMetadataRetriever.retrieve(opMetadataURL);
			} catch (IOException e) {
				Loggers.MAIN.info("Couldn't retrieve OP metadata for {}: {}", issURL, e.getMessage());
				new Notification(
					"OpenID Provider Metadata HTTP GET Failed",
					"<p><strong>" + opMetadataURL + "</strong></p><p>" + e.getMessage() + "</p>",
					Notification.Type.WARNING_MESSAGE,
					true)
					.show(currentPage);
				return;
			} catch (ParseException e) {
				Loggers.MAIN.info("Invalid OP metadata for {}: {}", issURL, e.getMessage());
				new Notification(
					"Invalid OpenID Provider Metadata",
					"<p><strong>" + opMetadataURL + "</strong></p><p>" + e.getMessage() + "</p>",
					Notification.Type.WARNING_MESSAGE,
					true)
					.show(currentPage);
				return;
			}
			
			Loggers.MAIN.info("Retrieved OP metadata: {}", opMetadata.toJSONObject());
			
			opMetadata.applyDefaults();
			
			// Set fields from OP data
			oidcParameters.getProviderProperties().getItemProperty("iss").setValue(opMetadata.getIssuer().getValue());
			
			oidcParameters.getProviderProperties().getItemProperty("jwks_uri").setValue(opMetadata.getJWKSetURI().toString());
			
			if (opMetadata.getAuthorizationEndpointURI() != null) {
				oidcParameters.getProviderProperties().getItemProperty("authz_uri").setValue(opMetadata.getAuthorizationEndpointURI().toString());
			}
			
			if (opMetadata.getTokenEndpointURI() != null) {
				oidcParameters.getProviderProperties().getItemProperty("token_uri").setValue(opMetadata.getTokenEndpointURI().toString());
			}
			
			if (opMetadata.getUserInfoEndpointURI() != null) {
				oidcParameters.getProviderProperties().getItemProperty("userinfo_uri").setValue(opMetadata.getUserInfoEndpointURI().toString());
			}
		});
		
		// Enable / disable manual OP endpoint entry
		endpointsEditManuallyCheckbox.addValueChangeListener((Property.ValueChangeListener) valueChangeEvent -> {
			if ((Boolean)valueChangeEvent.getProperty().getValue()) {
				opEndpoints.forEach(item -> item.setReadOnly(false));
			} else {
				opEndpoints.forEach(item -> item.setReadOnly(true));
			}
		});
		
		return form;
	}
	
	
	/**
	 * Creates the form to enter the OpenID relying party registration
	 * details.
	 *
	 * @return The form layout.
	 */
	private FormLayout createClientInformationForm() {
		
		FormLayout form = new FormLayout();
		form.setMargin(true);
		form.setSpacing(true);
		
		final int CLIENT_FIELD_COLS = 25;
		
		TextField clientIDField = new TextField("Client ID");
		clientIDField.setRequired(true);
		clientIDField.setColumns(CLIENT_FIELD_COLS);
		clientIDField.setPropertyDataSource(oidcParameters.getClientProperties().getItemProperty("client_id"));
		clientIDField.addValueChangeListener(valueChangeListener);
		form.addComponent(clientIDField);
		
		NativeSelect clientAuthSelect = new NativeSelect("Client authentication", Arrays.asList(
			ClientAuthenticationMethod.CLIENT_SECRET_BASIC.getValue(),
			ClientAuthenticationMethod.CLIENT_SECRET_POST.getValue(),
			ClientAuthenticationMethod.CLIENT_SECRET_JWT.getValue(),
			ClientAuthenticationMethod.NONE.getValue()));
		clientAuthSelect.setRequired(true);
		clientAuthSelect.setValue(ClientAuthenticationMethod.CLIENT_SECRET_BASIC.getValue());
		clientAuthSelect.setNullSelectionAllowed(false);
		clientAuthSelect.setPropertyDataSource(oidcParameters.getClientProperties().getItemProperty("token_endpoint_auth_method"));
		clientAuthSelect.addValueChangeListener(valueChangeListener);
		form.addComponent(clientAuthSelect);
		
		final NativeSelect jwsAuthSelect = new NativeSelect("JWT auth algorithm", Arrays.asList(
			JWSAlgorithm.HS256.getName(),
			JWSAlgorithm.HS384.getName(),
			JWSAlgorithm.HS512.getName()));
		jwsAuthSelect.setRequired(true);
		jwsAuthSelect.setValue(JWSAlgorithm.HS256.getName());
		jwsAuthSelect.setNullSelectionAllowed(false);
		jwsAuthSelect.setPropertyDataSource(oidcParameters.getClientProperties().getItemProperty("token_endpoint_auth_signing_alg"));
		jwsAuthSelect.setVisible(false);
		jwsAuthSelect.addValueChangeListener(valueChangeListener);
		form.addComponent(jwsAuthSelect);
		
		final TextField clientSecretField = new TextField("Client secret");
		clientSecretField.setRequired(true);
		clientSecretField.setColumns(CLIENT_FIELD_COLS);
		clientSecretField.setPropertyDataSource(oidcParameters.getClientProperties().getItemProperty("client_secret"));
		clientSecretField.addValueChangeListener(valueChangeListener);
		form.addComponent(clientSecretField);
		
		final NativeSelect pkceMethod = new NativeSelect("PKCE", Arrays.asList(
			"[ disable ]",
			CodeChallengeMethod.S256.getValue(),
			CodeChallengeMethod.PLAIN.getValue()));
		pkceMethod.setRequired(false);
		pkceMethod.setValue("[ disable ]");
		pkceMethod.setNullSelectionAllowed(false);
		pkceMethod.setPropertyDataSource(oidcParameters.getClientProperties().getItemProperty("code_challenge_method"));
		pkceMethod.setVisible(false);
		pkceMethod.addValueChangeListener(valueChangeListener);
		form.addComponent(pkceMethod);
		
		clientAuthSelect.addValueChangeListener((Property.ValueChangeListener) valueChangeEvent -> {
			if (valueChangeEvent.getProperty().getValue().equals(ClientAuthenticationMethod.NONE.getValue())) {
				clientSecretField.setRequired(false);
				clientSecretField.setVisible(false);
				pkceMethod.setVisible(true);
			} else {
				clientSecretField.setRequired(true);
				clientSecretField.setVisible(true);
				pkceMethod.setVisible(false);
			}
			if (valueChangeEvent.getProperty().getValue().equals(ClientAuthenticationMethod.CLIENT_SECRET_JWT.getValue())) {
				jwsAuthSelect.setRequired(true);
				jwsAuthSelect.setVisible(true);
			} else {
				jwsAuthSelect.setRequired(false);
				jwsAuthSelect.setVisible(false);
			}
		});
		
		
		TextField redirectionURI = new TextField("Redirection URI");
		redirectionURI.setRequired(true);
		redirectionURI.setColumns(CLIENT_FIELD_COLS);
		redirectionURI.setPropertyDataSource(oidcParameters.getClientProperties().getItemProperty("redirect_uri"));
		redirectionURI.addValueChangeListener(valueChangeListener);
		form.addComponent(redirectionURI);
		
		return form;
	}
	
	
	/**
	 * Creates the form to enter the OpenID authentication request details.
	 *
	 * @return The form layout.
	 */
	private FormLayout createAuthenticationRequestForm() {
		
		final FormLayout form = new FormLayout();
		form.setMargin(true);
		form.setSpacing(true);
		
		// Response type
		Collection<String> responseTypeChoices = new LinkedList<>();
		responseTypeChoices.add("code");
		responseTypeChoices.add("id_token");
		responseTypeChoices.add("code id_token");
		responseTypeChoices.add("id_token token");
		responseTypeChoices.add("code id_token token");
		
		final NativeSelect responseTypesSelect = new NativeSelect("Response type", responseTypeChoices);
		responseTypesSelect.setNullSelectionAllowed(false);
		responseTypesSelect.setPropertyDataSource(oidcParameters.getRequestProperties().getItemProperty("response_type"));
		responseTypesSelect.addValueChangeListener(valueChangeListener);
		form.addComponent(responseTypesSelect);
		
		// Scope
		final HorizontalLayout scopeValues = new HorizontalLayout();
		scopeValues.setCaption("Scope");
		scopeValues.setSpacing(true);
		
		CheckBox checkBox = new CheckBox("openid", oidcParameters.getRequestProperties().getItemProperty("scope_openid"));
		checkBox.setReadOnly(true);
		checkBox.addValueChangeListener(valueChangeListener);
		scopeValues.addComponent(checkBox);
		
		checkBox = new CheckBox("email", oidcParameters.getRequestProperties().getItemProperty("scope_email"));
		checkBox.addValueChangeListener(valueChangeListener);
		scopeValues.addComponent(checkBox);
		
		checkBox = new CheckBox("profile", oidcParameters.getRequestProperties().getItemProperty("scope_profile"));
		checkBox.addValueChangeListener(valueChangeListener);
		scopeValues.addComponent(checkBox);
		
		checkBox = new CheckBox("phone", oidcParameters.getRequestProperties().getItemProperty("scope_phone"));
		checkBox.addValueChangeListener(valueChangeListener);
		scopeValues.addComponent(checkBox);
		
		checkBox = new CheckBox("address", oidcParameters.getRequestProperties().getItemProperty("scope_address"));
		checkBox.addValueChangeListener(valueChangeListener);
		scopeValues.addComponent(checkBox);
		
		checkBox = new CheckBox("offline_access", oidcParameters.getRequestProperties().getItemProperty("scope_offline_access"));
		checkBox.addValueChangeListener(valueChangeListener);
		scopeValues.addComponent(checkBox);
		
		form.addComponent(scopeValues);
		
		// Optional parameters
		final List<Component> optionalParams = new LinkedList<>();
		
		final HorizontalLayout optionsLayout = new HorizontalLayout();
		optionsLayout.setCaption("Options");
		final Button optionsButton = new Button("Show");
		final AtomicBoolean optionsOn = new AtomicBoolean(false);
		optionsButton.addClickListener((Button.ClickListener) clickEvent -> {
			
			optionsOn.set(! optionsOn.get());
			
			if (optionsOn.get()) {
				for (Component c: optionalParams) {
					c.setVisible(true);
				}
				optionsButton.setCaption("Hide");
			} else {
				for (Component c: optionalParams) {
					c.setVisible(false);
				}
				
				optionsButton.setCaption("Show");
			}
		});
		optionsLayout.addComponent(optionsButton);
		form.addComponent(optionsLayout);
		
		// Optional prompt
		Collection<String> promptChoices = new LinkedList<>();
		promptChoices.add("default");
		promptChoices.add("none");
		promptChoices.add("login");
		promptChoices.add("consent");
		promptChoices.add("select_account");
		
		final NativeSelect promptSelect = new NativeSelect("Prompt", promptChoices);
		promptSelect.setNullSelectionAllowed(false);
		promptSelect.setPropertyDataSource(oidcParameters.getRequestProperties().getItemProperty("prompt"));
		promptSelect.addValueChangeListener(valueChangeListener);
		promptSelect.setVisible(false);
		optionalParams.add(promptSelect);
		form.addComponent(promptSelect);
		
		// Optional max auth age
		final TextField maxAge = new TextField();
		IntegerField.extend(maxAge);
		maxAge.setValue("-1");
		maxAge.setNullRepresentation("-1");
		maxAge.setPropertyDataSource(oidcParameters.getRequestProperties().getItemProperty("max_age"));
		maxAge.addValueChangeListener(valueChangeListener);
		
		Collection<TimeUnit> maxAgeTimeUnitChoices = new LinkedList<>();
		maxAgeTimeUnitChoices.add(TimeUnit.SECONDS);
		maxAgeTimeUnitChoices.add(TimeUnit.MINUTES);
		maxAgeTimeUnitChoices.add(TimeUnit.HOURS);
		maxAgeTimeUnitChoices.add(TimeUnit.DAYS);
		
		final NativeSelect maxAgeTimeUnitSelect = new NativeSelect();
		maxAgeTimeUnitSelect.addItems(maxAgeTimeUnitChoices);
		maxAgeTimeUnitSelect.setNullSelectionAllowed(false);
		maxAgeTimeUnitSelect.setPropertyDataSource(oidcParameters.getRequestProperties().getItemProperty("max_age_time_unit"));
		maxAgeTimeUnitSelect.addValueChangeListener(valueChangeListener);
		
		final HorizontalLayout maxAgeComponent = new HorizontalLayout();
		maxAgeComponent.setCaption("Max auth age");
		maxAgeComponent.setSpacing(true);
		maxAgeComponent.addComponent(maxAge);
		maxAgeComponent.setComponentAlignment(maxAge, Alignment.MIDDLE_LEFT);
		maxAgeComponent.addComponent(maxAgeTimeUnitSelect);
		maxAgeComponent.setComponentAlignment(maxAgeTimeUnitSelect, Alignment.MIDDLE_LEFT);
		maxAgeComponent.setVisible(false);
		optionalParams.add(maxAgeComponent);
		form.addComponent(maxAgeComponent);
		
		// Optional ID token hint
		final TextField idTokenHint = new TextField("ID token hint");
		idTokenHint.setNullRepresentation("");
		idTokenHint.setPropertyDataSource(oidcParameters.getRequestProperties().getItemProperty("id_token_hint"));
		idTokenHint.addValueChangeListener(valueChangeListener);
		idTokenHint.setVisible(false);
		optionalParams.add(idTokenHint);
		form.addComponent(idTokenHint);
		
		// Optional login hint
		final TextField loginHint = new TextField("Login hint");
		loginHint.setNullRepresentation("");
		loginHint.setPropertyDataSource(oidcParameters.getRequestProperties().getItemProperty("login_hint"));
		loginHint.addValueChangeListener(valueChangeListener);
		loginHint.setVisible(false);
		optionalParams.add(loginHint);
		form.addComponent(loginHint);
		
		// Optional response mode
		Collection<String> responseModeChoices = new LinkedList<>();
		responseModeChoices.add("default");
		responseModeChoices.add("query");
		responseModeChoices.add("fragment");
		responseModeChoices.add("form_post");
		
		final NativeSelect responseModeSelect = new NativeSelect("Response mode", responseModeChoices);
		responseModeSelect.setNullSelectionAllowed(false);
		responseModeSelect.setPropertyDataSource(oidcParameters.getRequestProperties().getItemProperty("response_mode"));
		responseModeSelect.addValueChangeListener(valueChangeListener);
		responseModeSelect.setVisible(false);
		optionalParams.add(responseModeSelect);
		form.addComponent(responseModeSelect);
		
		final Hr bottomHr = new Hr();
		form.addComponent(bottomHr);
		
		final Button makeRequestButton = new Button("Log in with OpenID Connect");
		popupOpener.setFeatures("width=800,height=600,scrollbars=1");
		popupOpener.extend(makeRequestButton);
		AuthenticationRequest authRequest = buildAuthenticationRequest();
		if (authRequest != null) {
			// Initial set
			String uriString = authRequest.toURI().toString();
			popupOpener.setUrl(uriString);
		}
		makeRequestButton.addClickListener((Button.ClickListener) clickEvent -> {
			// Rebuild request
			AuthenticationRequest authRequest1 = buildAuthenticationRequest();
			if (authRequest1 != null) {
				// Initial set
				String uriString = authRequest1.toURI().toString();
				popupOpener.setUrl(uriString);
			}
		});
		
		HorizontalLayout buttons = new HorizontalLayout();
		buttons.setSpacing(true);
		buttons.addComponent(makeRequestButton);
		
		form.addComponent(buttons);
		
		
		return form;
	}
	
	
	/**
	 * Builds an OpenID authentication request from the parameters entered
	 * in the provider, client and request forms.
	 */
	private AuthenticationRequest buildAuthenticationRequest() {
		
		try {
			// Compose the OP metadata object
			Issuer issuer = new Issuer((String)oidcParameters.getProviderProperties().getItemProperty("iss").getValue());
			List<SubjectType> subjectTypes = Collections.singletonList(SubjectType.PUBLIC);
			URI jwkSetURI = new URI((String)oidcParameters.getProviderProperties().getItemProperty("jwks_uri").getValue());
			
			OIDCProviderMetadata opMetadata = new OIDCProviderMetadata(issuer, subjectTypes, jwkSetURI);
			opMetadata.setAuthorizationEndpointURI(new URI((String)oidcParameters.getProviderProperties().getItemProperty("authz_uri").getValue()));
			opMetadata.setTokenEndpointURI(new URI((String)oidcParameters.getProviderProperties().getItemProperty("token_uri").getValue()));
			opMetadata.setUserInfoEndpointURI(new URI((String)oidcParameters.getProviderProperties().getItemProperty("userinfo_uri").getValue()));
			opMetadata.applyDefaults();
			
			// Set the client credentials and callback
			String clientIDString = (String)oidcParameters.getClientProperties().getItemProperty("client_id").getValue();
			if (StringUtils.isBlank(clientIDString)) {
				throw new Exception("Missing client_id");
			}
			ClientID clientID = new ClientID(clientIDString);
			
			String redirectURIString = (String)oidcParameters.getClientProperties().getItemProperty("redirect_uri").getValue();
			if (StringUtils.isBlank(redirectURIString)) {
				throw new Exception("Missing client redirect_uri");
			}
			URI redirectURI = new URI(redirectURIString);
			
			ClientAuthenticationMethod clientAuthMethod = ClientAuthenticationMethod.parse((String)oidcParameters.getClientProperties().getItemProperty("token_endpoint_auth_method").getValue());
			
			Secret clientSecret = null;
			if (! clientAuthMethod.equals(ClientAuthenticationMethod.NONE)) {
				String clientSecretString = (String)oidcParameters.getClientProperties().getItemProperty("client_secret").getValue();
				if (StringUtils.isBlank(clientSecretString)) {
					throw new Exception("Missing client_secret");
				}
				clientSecret = new Secret(clientSecretString);
			}
			
			JWSAlgorithm clientAuthJWS = null;
			if (clientAuthMethod.equals(ClientAuthenticationMethod.CLIENT_SECRET_JWT)) {
				clientAuthJWS = JWSAlgorithm.parse((String)oidcParameters.getClientProperties().getItemProperty("token_endpoint_auth_signing_alg").getValue());
			}
			
			CodeChallengeMethod codeChallengeMethod = null;
			if (clientAuthMethod.equals(ClientAuthenticationMethod.NONE)) {
				String pkceMethodString = (String)oidcParameters.getClientProperties().getItemProperty("code_challenge_method").getValue();
				if (! "[ disable ]".equals(pkceMethodString)) {
					codeChallengeMethod = CodeChallengeMethod.parse(pkceMethodString);
					Loggers.MAIN.info("PKCE method: " + codeChallengeMethod);
				}
			}
			
			OIDCClientMetadata clientMetadata = new OIDCClientMetadata();
			clientMetadata.setTokenEndpointAuthMethod(clientAuthMethod);
			clientMetadata.setTokenEndpointAuthJWSAlg(clientAuthJWS);
			clientMetadata.setRedirectionURI(redirectURI);
			OIDCClientInformation clientInfo = new OIDCClientInformation(clientID, null, clientMetadata, clientSecret);
			
			// Compose the OIDC auth request
			ResponseType responseType = ResponseType.parse((String)oidcParameters.getRequestProperties().getItemProperty("response_type").getValue());
			
			Scope scope = new Scope();
			
			if ((Boolean)oidcParameters.getRequestProperties().getItemProperty("scope_openid").getValue())
				scope.add("openid");
			
			if ((Boolean)oidcParameters.getRequestProperties().getItemProperty("scope_email").getValue())
				scope.add("email");
			
			if ((Boolean)oidcParameters.getRequestProperties().getItemProperty("scope_profile").getValue())
				scope.add("profile");
			
			if ((Boolean)oidcParameters.getRequestProperties().getItemProperty("scope_phone").getValue())
				scope.add("phone");
			
			if ((Boolean)oidcParameters.getRequestProperties().getItemProperty("scope_address").getValue())
				scope.add("address");
			
			if ((Boolean)oidcParameters.getRequestProperties().getItemProperty("scope_offline_access").getValue())
				scope.add("offline_access");
			
			String authzEndpointURIString = (String)oidcParameters.getProviderProperties().getItemProperty("authz_uri").getValue();
			
			if (StringUtils.isEmpty(authzEndpointURIString)) {
				throw new Exception("Missing authorization endpoint URI");
			}
			
			URI authzEndpointURI = new URI(authzEndpointURIString);
			
			String promptString = (String)oidcParameters.getRequestProperties().getItemProperty("prompt").getValue();
			
			Prompt prompt = promptString.equals("default") ? null : Prompt.parse(promptString);
			
			int maxAge = (Integer)oidcParameters.getRequestProperties().getItemProperty("max_age").getValue();
			
			TimeUnit maxAgeTimeUnit = (TimeUnit)oidcParameters.getRequestProperties().getItemProperty("max_age_time_unit").getValue();
			
			maxAge = new Long(maxAgeTimeUnit.toSeconds(maxAge)).intValue();
			
			String idTokenHintString = (String)oidcParameters.getRequestProperties().getItemProperty("id_token_hint").getValue();
			
			JWT idTokenHint = null;
			
			if (StringUtils.isNotBlank(idTokenHintString)) {
				try {
					idTokenHint = JWTParser.parse(idTokenHintString);
				} catch (java.text.ParseException e) {
					// ignore
				}
			}
			
			String loginHint = (String)oidcParameters.getRequestProperties().getItemProperty("login_hint").getValue();
			
			String responseModeString = (String)oidcParameters.getRequestProperties().getItemProperty("response_mode").getValue();
			
			ResponseMode responseMode = responseModeString.equals("default") ? null : new ResponseMode(responseModeString);
			
			CodeVerifier codeVerifier = null;
			CodeChallenge codeChallenge = null;
			
			if (codeChallengeMethod != null) {
				codeVerifier = new CodeVerifier();
				codeChallenge = CodeChallenge.compute(codeChallengeMethod, codeVerifier);
			}
			
			AuthenticationRequest authRequest = new AuthenticationRequest.Builder(
				responseType, scope, clientID, redirectURI)
				.responseMode(responseMode)
				.display(Display.POPUP)
				.state(new State())
				.nonce(new Nonce())
				.prompt(prompt)
				.maxAge(maxAge)
				.idTokenHint(idTokenHint)
				.loginHint(loginHint)
				.codeChallenge(codeChallenge, codeChallengeMethod)
				.endpointURI(authzEndpointURI)
				.build();
			
			// Save request to session so the popup window can fetch it
			pendingRequests.put(authRequest.getState(), new PendingAuthenticationRequest(opMetadata, clientInfo, authRequest, codeVerifier));
			
			return authRequest;
			
		} catch (Exception e) {
			Loggers.MAIN.info(e.getMessage());
			return null;
		}
	}
	
	
	/**
	 * Creates a footer for the window.
	 *
	 * @return The footer layout.
	 */
	private Layout createFooter() {
		
		Layout footer = new VerticalLayout();
		footer.addComponent(new Gap());
		Label text = new Label("<small>OpenID Connect 1.0 developer client | " +
				"<a href=\"https://bitbucket.org/connect2id/openid-connect-dev-client\">Git repo</a> | " +
				"Built with &hearts; by <a href=\"http://connect2id.com\">Connect2id</a> | &copy; 2012 - 2017</small>");
		text.setContentMode(ContentMode.HTML);
		footer.addComponent(text);
		return footer;
	}
	
	
	@Override
	protected void init(VaadinRequest request) {
		
		getPage().setTitle("OpenID Connect 1.0 Test Client");
		
		// The window content
		VerticalLayout content = new VerticalLayout();
		content.setMargin(true);
		content.setStyleName("oidc-client");
		
		Label title = new Label("<h1>OpenID Connect 1.0 Client</h1>");
		title.setContentMode(ContentMode.HTML);
		content.addComponent(title);
		
		Accordion panels = new Accordion();
		panels.setHeight("80%");
		panels.setCaptionAsHtml(true);
		
		Layout providerDetails = createProviderDetailsForm();
		panels.addTab(providerDetails, "OpenID provider details", new ThemeResource("images/openid-24x24.png"));
		
		Layout clientInformation = createClientInformationForm();
		panels.addTab(clientInformation, "Client details", new ThemeResource("images/client-24x24.png"));
		
		Layout authRequest = createAuthenticationRequestForm();
		panels.addTab(authRequest, "Authenticate end-user", new ThemeResource("images/user-24x24.png"));
		
		// The auth request panel is the default selected tab
		panels.setSelectedTab(authRequest);
		
		content.addComponent(panels);
		
		content.addComponent(createFooter());
		
		setContent(content);
	}
}
