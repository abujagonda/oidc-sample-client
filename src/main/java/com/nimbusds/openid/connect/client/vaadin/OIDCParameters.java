package com.nimbusds.openid.connect.client.vaadin;


import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.nimbusds.jose.JWSAlgorithm;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.VaadinServlet;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * OpenID Connect OP, RP and request parameters.
 */
class OIDCParameters {
	
	
	/**
	 * The initial client configuration properties, used to fill in the
	 * provider and client details form.
	 */
	private static final String CLIENT_PROPERTIES_FILE = "/WEB-INF/client.properties";
	
	
	/**
	 * The logger.
	 */
	private static final Logger log = LogManager.getLogger("MAIN");
	
	
	/**
	 * The OpenID Connect provider properties.
	 */
	private final PropertysetItem providerProps;
	
	
	/**
	 * The registered OpenID Connect client details.
	 */
	private final PropertysetItem clientProps;
	
	
	/**
	 * The OpenID Connect authentication request properties.
	 */
	private final PropertysetItem requestProps;
	
	
	/**
	 * Creates a new OpenID parameters instance.
	 */
	OIDCParameters() {
		
		
		Properties props = new Properties();
		
		try {
			props.load(VaadinServlet.getCurrent().getServletContext().getResourceAsStream(CLIENT_PROPERTIES_FILE));
			
		} catch (IOException e) {
			String msg = "Couldn't load client configuration: " + e.getMessage();
			log.fatal(msg, e);
			throw new RuntimeException(msg, e);
		}
		
		log.info("Initial client configuration properties:");
		for (String key: props.stringPropertyNames()) {
			log.info("\t> " + key + " = " + props.getProperty(key));
		}
		
		// OP details
		providerProps = new PropertysetItem();
		providerProps.addItemProperty("iss", new ObjectProperty<>(props.getProperty("op.iss"), String.class));
		providerProps.addItemProperty("jwks_uri", new ObjectProperty<>(props.getProperty("op.jwks_uri"), String.class));
		providerProps.addItemProperty("authz_uri", new ObjectProperty<>(props.getProperty("op.authz_uri"), String.class));
		providerProps.addItemProperty("token_uri", new ObjectProperty<>(props.getProperty("op.token_uri"), String.class));
		providerProps.addItemProperty("userinfo_uri", new ObjectProperty<>(props.getProperty("op.userinfo_uri"), String.class));
		
		// RP details
		clientProps = new PropertysetItem();
		clientProps.addItemProperty("client_id", new ObjectProperty<>(props.getProperty("rp.client_id"), String.class));
		clientProps.addItemProperty("client_secret", new ObjectProperty<>(props.getProperty("rp.client_secret"), String.class));
		clientProps.addItemProperty("redirect_uri", new ObjectProperty<>(props.getProperty("rp.redirect_uri"), String.class));
		clientProps.addItemProperty("token_endpoint_auth_method", new ObjectProperty<>(props.getProperty("rp.token_endpoint_auth_method"), String.class));
		clientProps.addItemProperty("token_endpoint_auth_signing_alg", new ObjectProperty<>(props.getProperty("rp.token_endpoint_auth_signing_alg"), String.class));
		if (StringUtils.isBlank((String)clientProps.getItemProperty("token_endpoint_auth_signing_alg").getValue())) {
			clientProps.getItemProperty("token_endpoint_auth_signing_alg").setValue(JWSAlgorithm.RS256.getName());
		}
		clientProps.addItemProperty("code_challenge_method", new ObjectProperty<>(props.getProperty("rp.code_challenge_method"), String.class));
		if (StringUtils.isBlank((String)clientProps.getItemProperty("code_challenge_method").getValue())) {
			clientProps.getItemProperty("code_challenge_method").setValue("[ disable ]");
		}
		
		// Request details
		requestProps = new PropertysetItem();
		requestProps.addItemProperty("response_type", new ObjectProperty<>("code", String.class));
		requestProps.addItemProperty("scope_openid", new ObjectProperty<>(true, Boolean.class));
		requestProps.addItemProperty("scope_email", new ObjectProperty<>(true, Boolean.class));
		requestProps.addItemProperty("scope_profile", new ObjectProperty<>(false, Boolean.class));
		requestProps.addItemProperty("scope_phone", new ObjectProperty<>(false, Boolean.class));
		requestProps.addItemProperty("scope_address", new ObjectProperty<>(false, Boolean.class));
		requestProps.addItemProperty("scope_offline_access", new ObjectProperty<>(false, Boolean.class));
		requestProps.addItemProperty("prompt", new ObjectProperty<>("default", String.class));
		requestProps.addItemProperty("max_age", new ObjectProperty<>(-1, Integer.class));
		requestProps.addItemProperty("max_age_time_unit", new ObjectProperty<>(TimeUnit.SECONDS, TimeUnit.class));
		requestProps.addItemProperty("id_token_hint", new ObjectProperty<>(null, String.class));
		requestProps.addItemProperty("login_hint", new ObjectProperty<>(null, String.class));
		requestProps.addItemProperty("response_mode", new ObjectProperty<>("default", String.class));
	}
	
	
	PropertysetItem getProviderProperties() {
		return providerProps;
	}
	
	
	PropertysetItem getClientProperties() {
		return clientProps;
	}
	
	
	PropertysetItem getRequestProperties() {
		return requestProps;
	}
	
	
	void clearProviderEndpoints() {
		
		providerProps.getItemProperty("jwks_uri").setValue("");
		providerProps.getItemProperty("authz_uri").setValue("");
		providerProps.getItemProperty("token_uri").setValue("");
		providerProps.getItemProperty("userinfo_uri").setValue("");
	}
}
