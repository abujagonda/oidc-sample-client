package com.nimbusds.openid.connect.client.vaadin;


import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import org.junit.Test;


public class OIDCProviderMetadataRetrieverTest {
	
	
	@Test
	public void testComposeURL()
		throws MalformedURLException {
		
		URL issuerURL = new URL("https://c2id.com");
		URL opMetadataURL = OIDCProviderMetadataRetriever.composeMetadataURL(issuerURL);
		assertEquals("https://c2id.com/.well-known/openid-configuration", opMetadataURL.toString());
	}
	

	@Test
	public void testLocalhost()
		throws Exception {
		
		URL issuerURL = new URL("https://demo.c2id.com/c2id");
		URL opMetadataURL = OIDCProviderMetadataRetriever.composeMetadataURL(issuerURL);
		
		OIDCProviderMetadata opMetadata = OIDCProviderMetadataRetriever.retrieve(opMetadataURL);
		
		assertNotNull(opMetadata);
	}
}
